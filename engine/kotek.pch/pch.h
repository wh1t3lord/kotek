#pragma once

#include <kotek.render.gl.glad3.3/include/kotek_render_gl_glad3.h>
#include <kotek.render.gl.glad4.6/include/kotek_render_gl_glad4.6.h>

#include <kotek.core/include/kotek_core.h>

// TODO: add support for shared files from any project (except the Core of
// course) like kotek.render.shared

#include <kotek.render.shared/include/kotek_render_shared.h>
#include <kotek.ui/include/kotek_ui.h>
 